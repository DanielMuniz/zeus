import React, { Component } from "react";
import { Link, animateScroll as scroll } from "react-scroll";

// Projeto futuro para tradução do sistema
// import br from "./images/br.png";
// import us from "./images/us.png";

export default class Navbar extends Component {
  scrollToTop = () => {
    scroll.scrollToTop();
  };

  render() {
    return (
      <nav className="nav" id="navbar">
        <div className="nav-content">
          <ul className="nav-items">
            <li className="nav-item">
              <Link
                activeClass="active"
                to="section1"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                <b className="title-content">{this.props.datapage.section1}</b>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                activeClass="active"
                to="section2"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                <b className="title-content">{this.props.datapage.section2}</b>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                activeClass="active"
                to="section3"
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                <b className="title-content">{this.props.datapage.section3}</b>
              </Link>
            </li>
            <li className="nav-item">
              {/* <img
                onClick={(e) => this.props.setLanguage(e, "en")}
                className="float-right language language-primary"
                src={us}
                alt="flag usa"
              />
              <img
                onClick={(e) => this.props.setLanguage(e, "pt")}
                className="float-right language"
                src={br}
                alt="bandeira brasil"
              /> */}
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
