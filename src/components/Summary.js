import React, { Component } from "react";
import { Row } from "reactstrap";

export default class Summary extends Component {
  state = {
    hover: false,
  };

  render() {

    return (
      <div>
        <Row>
          <h1 className="summary">
            Periodic table, in full periodic table of the elements, in chemistry, the organized array of all the chemical elements in order of increasing atomic number—i.e., the total number of protons in the atomic nucleus. When the chemical elements are thus arranged, there is a recurring pattern called the “periodic law” in their properties, in which elements in the same column (group) have similar properties. The initial discovery, which was made by Dmitry I. Mendeleyev in the mid-19th century, has been of inestimable value in the development of chemistry.

            It was not actually recognized until the second decade of the 20th century that the order of elements in the periodic system is that of their atomic numbers, the integers of which are equal to the positive electrical charges of the atomic nuclei expressed in electronic units. In subsequent years great progress was made in explaining the periodic law in terms of the electronic structure of atoms and molecules. This clarification has increased the value of the law, which is used as much today as it was at the beginning of the 20th century, when it expressed the only known relationship among the elements.
          </h1>
        </Row>
      </div>
    );
  }
}
