import React, { Component } from "react";
import JSONPretty from 'react-json-pretty';

export default class JsonPrint extends Component {
  state = {
    hover: false,
  };

  render() {

    var theme = {
      "main": "line-height:1.3;color:#66d9ef;background:#272822;overflow:auto; max-width:100%;max-height:400px;", 
      "error": "line-height:1.3;color:#66d9ef;background:#272822;overflow:auto;", 
      "key": "color:#f92672;", 
      "string": "color:#fd971f;", 
      "value": "color:#a6e22e;", 
      "boolean": "color:#ac81fe;"
    };

    return (
      <div>
        <JSONPretty theme={theme} data={this.props.content} />
      </div>
    );
  }
}
