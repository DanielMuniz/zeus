import React from "react";

export default function Section({ title = null, subtitle, dark, id }) {
  let titleText = "";

  if (title) titleText = <h1 className="title-content text-center">{title}</h1>;

  return (
    <div className={"section" + (dark ? " section-dark" : "")}>
      <div className="section-content" id={id}>
        {titleText}
        <p>{subtitle}</p>
      </div>
    </div>
  );
}
