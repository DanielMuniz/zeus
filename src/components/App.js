import React, { Component } from "react";
import { elementsEN, datapageEN } from "./elementsEN";
import { elementsPT } from "./elementsPT";
import Element from "./Element";
import { FaDiscord, FaEnvelope, FaLinkedin, FaGitlab, FaClipboard } from "react-icons/fa";
import { Row, Col, Container } from "reactstrap";
import Navbar from "./Navbar";
import Summary from "./Summary";
import Section from "./Section";
import JsonPrint from "./JsonPrint";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      withTarget: false,
      modal: false,
      element: {
        name: "",
        summary: "",
        symbol: "",
        category: "",
        number: "",
        source: "",
        appearance: "",
        atomic_mass: "",
        molar_heat: "",
        density: "",
        melt: "",
        boil: "",
        shells: [],
      },
      elementsEN: elementsEN,
      elementsPT: elementsPT,
      json: elementsEN,
      datapage: datapageEN,
      title: "",
      subtitle: "",
      languague: "en",
      nav: "",
    };
  }

  state = {
    showInfo: false,
    element: {},
  };

  showInfo = (data) => {
    this.setState({ showInfo: true, element: data, modal: !this.state.modal });
  };

  setLanguage = (e, lang) => {
    let languageOptions = document.querySelectorAll(".language");

    languageOptions[0].classList.remove("language-primary");
    languageOptions[1].classList.remove("language-primary");

    e.target.classList.add("language-primary");

    this.setState({
      showInfo: true,
      languague: lang,
      element: {},
    });
  };

  render() {
    let cell = [];
    let shells = [];
    let key = 0;

    let content = this.state.elementsEN;
    let elementShells = this.state.element.shells;

    if (this.state.languague === "pt") {
      content = this.state.elementsPT;
    }

    document.title = this.state.datapage.title;

    if (elementShells) {
      for (let shell = 0; shell < elementShells.length; shell++)
        shells.push(<div key={shell}>{elementShells[shell]}</div>);
    }

    let table = [];

    for (let row = 0; row < content.length; row++, key++) {
      for (let col = 0; col < content[row].length; col++, key++){

        table.push({number: content[row][col].number, name: content[row][col].name});

        cell.push(
          <Element
            showInfo={this.showInfo}
            data={content[row][col]}
            key={key}
          />
        );
      }
    }

    return (
      <div>
        <Navbar datapage={this.state.datapage} setLanguage={this.setLanguage} />

        <Section
          title={this.state.datapage.section1}
          subtitle=""
          dark={false}
          id="section1"
        />
        <div className="wrapper">
          <Row>
            <Col xs="12">
              <div id="table">

                {cell}

                <div
                  id="element-box"
                  className={
                    this.state.element.category
                      ? `shadow ${this.state.element.category}`
                      : ""
                  }
                >
                  <Row>
                    <Col xs="8">
                      <Col xs="7">
                        <div className="symbol">
                          {this.state.element.symbol}
                        </div>
                      </Col>
                      <Col xs="5">
                        <div className="number">
                          {this.state.element.number}
                        </div>
                      </Col>
                      <Col xs="10">
                        <div className="element-name">
                          {this.state.element.name}
                        </div>
                      </Col>
                    </Col>
                    <Col xs="4">
                      <div id="shells">{shells}</div>
                    </Col>
                  </Row>
                </div>
                <div id="information" className="style-2">
                  {this.state.element.summary}
                </div>
                <div id="details">
                  <span className="badge badge-pill badge-secondary">
                    {this.state.element.category}
                  </span>
                  <span className="badge badge-pill badge-secondary ml-2">
                    {this.state.element.phase}
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </div>

        <hr />

        <Section
          title={this.state.datapage.section2}
          subtitle=""
          dark={false}
          id="section2"
        />

        <div className="wrapper">
          <Summary />
        </div>

        <hr />

        <Section
          title={this.state.datapage.section3}
          subtitle=""
          dark={false}
          id="section3"
        />

        <div>
        <Col sm="12" md={{ size: 8, offset: 2 }}>
            <FaClipboard className="hoverIcon" onClick={() => {navigator.clipboard.writeText(JSON.stringify(this.state.json, null, 2))}} />
            <JsonPrint content={this.state.json} />
          </Col>
        </div>

        <hr />

        <footer className="footer">
          <Container>
            <Row>
              <Col className="mt-4" xs="5">
                <h5>
                  Periodic Table of Elements and their respective categories.
                  For suggestions contact me
                </h5>
              </Col>
              <Col xs="2"></Col>
              <Col className="mt-4" xs="5">
                <h4>
                  <FaDiscord className="mr-3" />
                  <FaEnvelope className="mr-3" />
                  <FaLinkedin className="mr-3" />
                  <FaGitlab className="mr-3" />
                </h4>
                <small className="text-small">daniel.muniz997@gmail.com</small>
              </Col>
            </Row>
            <hr />
            <Row>
              <Col xs="12">
                <div className="text-center">
                  Copyright @2019 |{" "}
                  <small className="">{this.state.subtitle}Daniel Muniz</small>
                </div>
              </Col>
            </Row>
          </Container>
        </footer>
      </div>
    );
  }
}

export default App;
